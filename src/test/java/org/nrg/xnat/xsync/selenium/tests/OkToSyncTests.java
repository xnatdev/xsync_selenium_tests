package org.nrg.xnat.xsync.selenium.tests;

import org.nrg.testing.xnat.conf.Settings;
import org.nrg.selenium.annotations.DisableWebDriver;
import org.nrg.testing.file.FileIO;
import org.nrg.xnat.pojo.Subject;
import org.nrg.xnat.pojo.experiments.ImagingSession;
import org.nrg.xnat.pojo.experiments.sessions.MRSession;
import org.nrg.xnat.xsync.selenium.BaseXSyncTest;
import org.testng.annotations.Test;

import java.nio.file.Paths;

public class OkToSyncTests extends BaseXSyncTest {

    private Subject subject1 = new Subject().label("SPP_0x960c08");

    private ImagingSession subject1_mr1 = new MRSession("SPP_0x960c08_MR1").subject(subject1);
    private ImagingSession subject1_mr2 = new MRSession("SPP_0x960c08_MR2").subject(subject1);

    @Test
    @DisableWebDriver
    public void testOkToSyncSkips() {
        final String config = "xsync_ok_to_sync_config.json";

        createProjects();

        final String configContents = FileIO.readFile(Paths.get(Settings.DATA_LOCATION, config)).
                replace("$SOURCE_PROJECT", sourceProject.getId()).replace("$DESTINATION_PROJECT", destinationProject.getId()).replace("$DESTINATION_XNAT", destinationXnat.getXnatUrl());
        sourceXnat.getSeleniumCredentials().expect().statusCode(200).given().body(configContents).post(xnatDriver.formatXapiUrl("/xsync/setup/projects/", sourceProject.getId()));
        driver.navigate().refresh(); // Refresh page to access XSync config which was POSTed since page was loaded.
        captureScreenshotlessStep();
        provideSyncCredentials(destinationXnat.getXnatUrl(), destinationXnat.getSeleniumUser());
        uploadAndNoteSessions(sourceProject, subject1_mr1, subject1_mr2);
        projectSync();
        waitForSyncCompletion(60);
        assertSessionsSyncedAndVerified(sourceProject);

        logLogout();
    }

    @Test
    public void testOkToSyncSuccessfulSync() {
        seleniumLogin();
        locateSession(subject1_mr1);
        markOkToSync();
        // TODO: finish
    }


}
