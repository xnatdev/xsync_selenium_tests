package org.nrg.xnat.xsync.selenium.tests;

import org.nrg.selenium.annotations.DisableWebDriver;
import org.nrg.selenium.annotations.HardDependency;
import org.nrg.testing.annotations.JiraKey;
import org.nrg.xnat.pojo.Subject;
import org.nrg.xnat.pojo.experiments.sessions.CTSession;
import org.nrg.xnat.pojo.experiments.sessions.MRSession;
import org.nrg.xnat.xsync.selenium.BaseXSyncTest;
import org.nrg.xnat.xsync.selenium.SyncResult;
import org.nrg.xnat.xsync.selenium.XSyncConfig;
import org.nrg.xnat.xsync.selenium.enums.OverallStatus;
import org.nrg.xnat.xsync.selenium.enums.SessionStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

public class BasicXSyncConfigTests extends BaseXSyncTest {

    private XSyncConfig basicConfig;
    private final double comparisonTolerance = 0.3;

    private Subject subject1 = new Subject().label("SPP_0xff2fb3");
    private Subject subject2 = new Subject().label("SPP_0x5533ff");
    private Subject subject3 = new Subject().label("C0");
    private Subject subject4 = new Subject().label("SPP_0x960c08");
    private Subject subject5 = new Subject().label("SPP_0x220790");

    private MRSession subj1_mr1 = new MRSession("SPP_0xff2fb3_MRI").subject(subject1);
    private MRSession subj2_mr1 = new MRSession("SPP_0x5533ff_MR1").subject(subject2);
    private CTSession subj1_ct1 = new CTSession("SPP_0xff2fb3_CT").subject(subject1);
    private MRSession subj2_mr2 = new MRSession("SPP_0x5533ff_MR2").subject(subject2);
    private MRSession subj3_mr1 = new MRSession("C01448_MR").subject(subject3); // does not match DICOM
    private MRSession subj3_mr2 = new MRSession("C01456_MR").subject(subject3); // does not match DICOM
    private MRSession subj3_mr3 = new MRSession("C01459_MR").subject(subject3); // does not match DICOM
    private MRSession subj3_mr3_dest = new MRSession("C01459_MR").subject(subject3); // does not match DICOM
    private MRSession subj4_mr1 = new MRSession("SPP_0x960c08_MR1").subject(subject4);
    private MRSession subj4_mr2 = new MRSession("SPP_0x960c08_MR2").subject(subject4);
    private MRSession subj5_mr1 = new MRSession("SPP_0x220790_MR1").subject(subject5);
    private MRSession subj5_mr2 = new MRSession("SPP_0x220790_MR2").subject(subject5);

    @BeforeClass
    public void setupConfig() {
        basicConfig = new XSyncConfig().destinationXnat(destinationXnat.getXnatUrl()).destinationProject(destinationProject);
    }

    @Test
    @DisableWebDriver // driver would be immediately closed to open driver for destination XNAT
    @JiraKey(simpleKey = "QA-492")
    public void testBasicSync() {
        createProjects();
        setupNewXSyncConfig(basicConfig, destinationXnat.getSeleniumUser());

        uploadAndNoteSessions(sourceProject, subj1_mr1, subj2_mr1, subj1_ct1);
        projectSync();
        waitForSyncCompletion(600);
        assertSessionsSyncedAndVerified(sourceProject, subj1_mr1, subj2_mr1, subj1_ct1);
        logLogout();
    }

    @Test
    @HardDependency("testBasicSync")
    @JiraKey(simpleKey = "QA-493")
    public void testBasicIgnoredSourceChanges() {
        final String scanNote = "XSync scan note.";
        final String sessionNote = "XSync session note.";
        final String originalScanQuality = "usable";
        final String scanQuality = "questionable";

        uploadAndNoteSessions(sourceProject, subj2_mr2, subj3_mr1);
        seleniumLogin();
        locateSession(subj1_mr1);

        exceptionSafeClick(locators.EDIT_LINK);
        xnatDriver.exceptionSafeSelect(locators.sessionEditNthQuality(1), scanQuality);
        xnatDriver.fill(locators.sessionEditNthNote(1), scanNote);
        xnatDriver.fill(locators.SESSION_EDIT_NOTES, sessionNote);
        uploadTargetedScreenshot(locators.SESSION_EDIT_SCAN_TBODY, locators.SESSION_EDIT_NOTES);
        exceptionSafeClick(locators.SESSION_EDIT_SUBMIT_BUTTON);
        xnatDriver.waitForElement(locators.SESSION_TITLE);
        xnatDriver.assertElementText(sessionNote, locators.SESSION_NOTES);
        xnatDriver.assertElementText(scanQuality, locators.sessionReportNthQuality(1));
        xnatDriver.assertElementText(scanNote, locators.sessionReportNthNote(1));
        captureStep(locators.SESSION_TITLE, locators.ARCHIVE_SCANS_LIST);
        xnatDriver.findBySearch(sourceProject.getId());
        projectSync();

        waitForSyncCompletion(600);
        assertSessionsSyncedAndVerified(sourceProject, subj2_mr2, subj3_mr1);
        logLogout();

        constructXnatDriver(destinationXnat);
        seleniumLogin();
        subj1_mr1.project(destinationProject);
        locateSession(subj1_mr1, false);
        xnatDriver.waitForElement(locators.SESSION_TITLE);
        xnatDriver.assertElementText(originalScanQuality, locators.sessionReportNthQuality(1));
        xnatDriver.assertElementText("", locators.sessionReportNthNote(1));
        xnatDriver.assertElementNotPresent(locators.SESSION_NOTES);
        captureStep(locators.SESSION_TITLE, locators.ARCHIVE_SCANS_LIST);
        logLogout();
    }

    @Test
    @HardDependency("testBasicIgnoredSourceChanges")
    @DisableWebDriver // will construct later
    @JiraKey(simpleKey = "QA-494")
    public void testBasicSyncConflictSkip() {
        final String sessionNote = "XSync session note.";

        uploadAndNoteSessions(sourceProject, subj3_mr2, subj3_mr3);

        constructXnatDriver(destinationXnat);
        subj3_mr3_dest.project(destinationProject);
        xnatDriver.uploadToSessionZipImporter(getXSyncData("W32"), subj3_mr3_dest);
        LOGGER.info(String.format("Session %s uploaded to session zip importer...", subj3_mr3_dest.getLabel()));
        captureScreenshotlessStep();

        seleniumLogin();
        locateSession(subj3_mr3_dest);

        exceptionSafeClick(locators.EDIT_LINK);
        xnatDriver.fill(locators.SESSION_EDIT_NOTES, sessionNote);
        uploadTargetedScreenshot(locators.SESSION_EDIT_SCAN_TBODY, locators.SESSION_EDIT_NOTES);
        exceptionSafeClick(locators.SESSION_EDIT_SUBMIT_BUTTON);
        xnatDriver.waitForElement(locators.SESSION_TITLE);
        xnatDriver.assertElementText(sessionNote, locators.SESSION_NOTES);
        captureStep(locators.SESSION_TITLE, locators.ARCHIVE_SCANS_LIST);
        logLogout();

        constructXnatDriver();
        seleniumLogin();
        xnatDriver.findBySearch(sourceProject.getId());
        projectSync();
        waitForSyncCompletion(600);
        final SyncResult syncResult = new SyncResult().sourceProject(sourceProject).overallStatus(OverallStatus.COMPLETE);
        syncResult.sessionResult(subj3_mr2, SessionStatus.SYNCED_AND_VERIFIED).sessionResult(subj3_mr3, SessionStatus.SKIPPED);
        assertSyncResult(syncResult);
        logLogout();

        constructXnatDriver(destinationXnat);

        seleniumLogin();
        locateSession(subj3_mr3_dest, false);
        xnatDriver.waitForElement(locators.SESSION_TITLE);
        xnatDriver.assertElementText(sessionNote, locators.SESSION_NOTES);
        xnatDriver.assertElementText("AX_T1_MPRAGE_3D", locators.sessionReportNthScanType(1));
        xnatDriver.assertSessionFiles(12, 47.7, "MB", 112, comparisonTolerance);
        captureStep(locators.SESSION_TITLE, locators.ARCHIVE_SCANS_LIST);
        logLogout();
    }

    @Test
    @HardDependency("testBasicSyncConflictSkip")
    @JiraKey(simpleKey = "QA-495")
    public void testBasicManualSessionSyncOverwrite() {
        seleniumLogin();

        locateSession(subj3_mr3);
        manualSessionSync(subj3_mr3);

        xnatDriver.findBySearch(sourceProject.getId());
        waitForSyncCompletion(300);
        final SyncResult syncResult = new SyncResult().sourceProject(sourceProject).overallStatus(OverallStatus.COMPLETE_NOT_VERIFIED);
        syncResult.sessionResult(subj3_mr3, SessionStatus.SYNCED_AND_VERIFIED);
        assertSyncResult(syncResult);
        logLogout();

        constructXnatDriver(destinationXnat);
        seleniumLogin();
        locateSession(subj3_mr3_dest);
        xnatDriver.waitForElement(locators.SESSION_TITLE);
        xnatDriver.assertElementNotPresent(locators.SESSION_NOTES);
        xnatDriver.assertElementText("t1_mprage_sag_novo", locators.sessionReportNthScanType(1));
        xnatDriver.assertSessionFiles(13, 108.6, "MB", 192, comparisonTolerance);

        logLogout();
    }

    @Test
    @HardDependency("testBasicManualSessionSyncOverwrite")
    @JiraKey(simpleKey = "QA-496")
    public void testBasicIgnoreChangesAfterManualSync() {
        final String addedSessionNotes = "Notes for XSync test.";

        uploadAndNoteSessions(sourceProject, subj4_mr1, subj4_mr2);

        seleniumLogin();

        locateSession(subj4_mr1);
        manualSessionSync(subj4_mr1);
        xnatDriver.findBySearch(sourceProject.getId());
        waitForSyncCompletion(300);
        final SyncResult syncResult = new SyncResult().sourceProject(sourceProject).overallStatus(OverallStatus.COMPLETE_NOT_VERIFIED);
        syncResult.sessionResult(subj4_mr1, SessionStatus.SYNCED_AND_VERIFIED);
        assertSyncResult(syncResult);

        locateSession(subj4_mr1);
        exceptionSafeClick(locators.EDIT_LINK);
        xnatDriver.fill(locators.SESSION_EDIT_NOTES, addedSessionNotes);
        uploadTargetedScreenshot(locators.SESSION_EDIT_SCAN_TBODY, locators.SESSION_EDIT_NOTES);
        exceptionSafeClick(locators.SESSION_EDIT_SUBMIT_BUTTON);
        xnatDriver.waitForElement(locators.SESSION_TITLE);
        xnatDriver.assertElementText(addedSessionNotes, locators.SESSION_NOTES);
        captureStep(locators.SESSION_TITLE, locators.ARCHIVE_SCANS_LIST);

        xnatDriver.findBySearch(sourceProject.getId());
        projectSync();
        waitForSyncCompletion(300);
        assertSessionsSyncedAndVerified(sourceProject, subj4_mr2);
        logLogout();

        constructXnatDriver(destinationXnat);
        seleniumLogin();
        locateSession(subj4_mr1, false);
        xnatDriver.waitForElement(locators.SESSION_TITLE);
        xnatDriver.assertElementNotPresent(locators.SESSION_NOTES);
        captureStep(locators.SESSION_TITLE, locators.ARCHIVE_SCANS_LIST);
        logLogout();
    }

    @Test
    @HardDependency("testBasicManualSessionSyncOverwrite")
    @JiraKey(simpleKey = "QA-497")
    public void testBasicConcurrentSyncsBlocked() {
        uploadAndNoteSessions(sourceProject, subj5_mr1, subj5_mr2);

        seleniumLogin();
        xnatDriver.findBySearch(sourceProject.getId());
        projectSync();

        launchProjectSync();
        assertTrue(driver.findElement(new ByChained(xsyncLocators.PROJECT_SYNC_ERROR_BANNER, By.tagName("b"))).getText().contains("Sync is currently running"));
        try {
            captureStep(xsyncLocators.PROJECT_SYNC_ERROR_BANNER);
        } catch (Exception e){
            captureScreenshotlessStep(); // If the save success banner disappears before we can screenshot it, just skip the screenshot
        }
        xnatDriver.waitForNotElement(xsyncLocators.PROJECT_SYNC_ERROR_BANNER);
        logLogout();
    }

}
