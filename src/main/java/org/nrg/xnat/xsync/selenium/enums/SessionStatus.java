package org.nrg.xnat.xsync.selenium.enums;

public enum SessionStatus {
    SYNCED_AND_VERIFIED ("SYNCED_AND_VERIFIED"),
    SKIPPED             ("SKIPPED");

    private String displayName;

    SessionStatus(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
