package org.nrg.xnat.xsync.selenium.enums;

public enum OverallStatus {
    COMPLETE_AND_VERIFIED ("Complete [Verified]"),
    COMPLETE_NOT_VERIFIED ("Complete [NOT Verified]"),
    COMPLETE              ("Complete");

    private String displayName;

    OverallStatus(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
