package org.nrg.xnat.xsync.selenium.enums;

public enum SyncType {
    ALL     ("All", "all"),
    NONE    ("None", "none"),
    INCLUDE ("Include", "include"),
    EXCLUDE ("Exclude", "exclude");

    private String displayName;
    private String jsonName;

    SyncType(String displayName, String jsonName) {
        this.displayName = displayName;
        this.jsonName = jsonName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getJsonName() {
        return jsonName;
    }

}
