package org.nrg.xnat.xsync.selenium;

import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.xsync.selenium.enums.Identifiers;
import org.nrg.xnat.xsync.selenium.enums.SyncFrequency;
import org.nrg.xnat.xsync.selenium.enums.SyncType;

import static org.nrg.xnat.xsync.selenium.enums.SyncType.*;

public class XSyncConfig {

    private boolean enabled = true;
    private SyncType projectResourceSyncType = NONE;
    private SyncType subjectResourceSyncType = NONE;
    private SyncType subjectAssessorSyncType = NONE;
    private SyncType imagingSessionSyncType = ALL;
    private SyncFrequency syncFrequency = SyncFrequency.WEEKLY;
    private Identifiers identifiers = Identifiers.USE_LOCAL;
    private boolean syncNewOnly = true;
    private boolean anonymize = false;
    private Project sourceProject;
    private Project destinationProject;
    private String destinationXnat;

    public boolean isEnabled() {
        return enabled;
    }

    public SyncType getProjectResourceSyncType() {
        return projectResourceSyncType;
    }

    public SyncType getSubjectResourceSyncType() {
        return subjectResourceSyncType;
    }

    public SyncType getSubjectAssessorSyncType() {
        return subjectAssessorSyncType;
    }

    public SyncType getImagingSessionSyncType() {
        return imagingSessionSyncType;
    }

    public SyncFrequency getSyncFrequency() {
        return syncFrequency;
    }

    public Identifiers getIdentifiers() {
        return identifiers;
    }

    public boolean isSyncNewOnly() {
        return syncNewOnly;
    }

    public boolean isAnonymize() {
        return anonymize;
    }

    public Project getSourceProject() {
        return sourceProject;
    }

    public Project getDestinationProject() {
        return destinationProject;
    }

    public String getDestinationXnat() {
        return destinationXnat;
    }

    public XSyncConfig enabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public XSyncConfig projectResourceSyncType(SyncType type) {
        this.projectResourceSyncType = type;
        return this;
    }

    public XSyncConfig subjectResourceSyncType(SyncType type) {
        this.subjectResourceSyncType = type;
        return this;
    }

    public XSyncConfig subjectAssessorSyncType(SyncType type) {
        this.subjectAssessorSyncType = type;
        return this;
    }

    public XSyncConfig imagingSessionSyncType(SyncType type) {
        this.imagingSessionSyncType = type;
        return this;
    }

    public XSyncConfig syncFrequency(SyncFrequency syncFrequency) {
        this.syncFrequency = syncFrequency;
        return this;
    }

    public XSyncConfig syncNewOnly(boolean value) {
        syncNewOnly = value;
        return this;
    }

    public XSyncConfig anonymize(boolean value) {
        anonymize = value;
        return this;
    }

    public XSyncConfig sourceProject(Project sourceProject) {
        this.sourceProject = sourceProject;
        return this;
    }

    public XSyncConfig destinationProject(Project destinationProject) {
        this.destinationProject = destinationProject;
        return this;
    }

    public XSyncConfig destinationXnat(String destinationXnat) {
        this.destinationXnat = destinationXnat;
        return this;
    }

}
