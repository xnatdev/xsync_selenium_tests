package org.nrg.xnat.xsync.selenium;

import com.google.common.base.Joiner;
import org.apache.commons.lang3.time.StopWatch;
import org.nrg.selenium.BaseSeleniumTest;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.selenium.auth.Credentials;
import org.nrg.testing.enums.TestData;
import org.nrg.selenium.util.AssortedUtils;
import org.nrg.testing.file.FileIO;
import org.nrg.selenium.xnat.XnatConfig;
import org.nrg.selenium.xnat.popups.XModal;
import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.User;
import org.nrg.xnat.pojo.experiments.ImagingSession;
import org.nrg.xnat.xsync.selenium.enums.SessionStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.nio.file.Paths;
import java.util.*;

import static org.testng.AssertJUnit.*;

public class BaseXSyncTest extends BaseSeleniumTest {

    protected XnatConfig sourceXnat;
    protected XnatConfig destinationXnat;
    protected Project sourceProject;
    protected Project destinationProject;
    protected XSyncLocators xsyncLocators = new XSyncLocators();

    public void downloadXSyncData() {
        FileIO.getTestData(TestData.XSYNC_DATA.getZipName());
        FileIO.unzip(Settings.DATA_LOCATION, TestData.XSYNC_DATA.getZipName());
    }

    public void setupXnats() {
        if (Settings.OTHER_XNAT_CONFIGS.isEmpty()) {
            throw new RuntimeException("XSync tests require a destination server (which could be the same as the source server). Specify the destination server properties using the xnat2 prefix instead of xnat (for example xnat2.selenium.user).");
        }

        sourceXnat = Settings.DEFAULT_XNAT_CONFIG;
        destinationXnat = Settings.OTHER_XNAT_CONFIGS.get(0);
        LOGGER.info(String.format("Source and destination XSync configs found: source = %s, destination = %s.", sourceXnat.getXnatUrl(), destinationXnat.getXnatUrl()));

        // source init and setup should already be handled by nrg_selenium, so init destination XNAT if needed

        if (destinationXnat.getInitSetting()) {
            constructXnatDriver(destinationXnat);
            xnatDriver.setupXnat();
        } else {
            constructXnatDriver(true, destinationXnat);
            AssortedUtils.checkAccount(xnatDriver, Credentials.build(destinationXnat.getSeleniumUser()), "Destination server selenium account");
        }
    }

    @BeforeSuite
    public void setupXSyncTests() {
        downloadXSyncData();
        setupXnats();
    }

    @BeforeClass()
    public void randomizeClassProjects() {
        sourceProject = new Project("XSYNC_" + RandomHelper.randomID(6));
        destinationProject = new Project("XSYNC_" + RandomHelper.randomID(6));
    }

    protected File getXSyncData(ImagingSession session) {
        return getXSyncData(session.getLabel());
    }

    protected File getXSyncData(String session) {
        return Paths.get(Settings.DATA_LOCATION, TestData.XSYNC_DATA.getName(), session + ".zip").toFile();
    }

    // Leaves you logged in on the source project when done
    protected void createProjects() {
        constructXnatDriver(destinationXnat);
        seleniumLogin();
        xnatDriver.createProject(destinationProject);
        logLogout();

        constructXnatDriver();
        seleniumLogin();
        xnatDriver.createProject(sourceProject);
    }

    protected void uploadAndNoteSessions(Project project, ImagingSession... sessions) {
        LOGGER.info("Uploading sessions through the session zip importer...");
        final List<String> sessionLabels = new ArrayList<>();
        for (ImagingSession session : sessions) {
            session.project(project);
            xnatDriver.uploadToSessionZipImporter(getXSyncData(session), session);
            restDriver.waitForAutoRun(session);
            LOGGER.info(String.format("Session %s uploaded to session zip importer...", session.getLabel()));
            sessionLabels.add(session.getLabel());
        }
        xnatDriver.commentStep("Uploaded sessions: " + Joiner.on(", ").join(sessionLabels));
        captureScreenshotlessStep();
    }

    protected void setupNewXSyncConfig(XSyncConfig xSyncConfig, User destinationUser) {
        xnatDriver.loadManageTab(xsyncLocators.XSYNC_MANAGE_TAB);
        exceptionSafeClick(xsyncLocators.BEGIN_CONFIGURATION);
        xnatDriver.performCheckBox(xsyncLocators.XSYNC_CONFIG_ENABLED, xSyncConfig.isEnabled());
        xnatDriver.performCheckBox(xsyncLocators.XSYNC_CONFIG_NEW_ONLY, xSyncConfig.isSyncNewOnly());
        xnatDriver.fill(xsyncLocators.XSYNC_CONFIG_DESTINATION_XNAT, xSyncConfig.getDestinationXnat());
        xnatDriver.fill(xsyncLocators.XSYNC_CONFIG_DESTINATION_PROJECT, xSyncConfig.getDestinationProject().getId());
        xnatDriver.exceptionSafeSelect(xsyncLocators.XSYNC_CONFIG_SYNC_FREQUENCY, xSyncConfig.getSyncFrequency().getDisplayName());
        xnatDriver.exceptionSafeSelect(xsyncLocators.XSYNC_CONFIG_IDENTIFIERS, xSyncConfig.getIdentifiers().getDisplayName());
        xnatDriver.performCheckBox(xsyncLocators.XSYNC_CONFIG_ANONYMIZE, xSyncConfig.isAnonymize());
        xnatDriver.exceptionSafeSelect(xsyncLocators.XSYNC_CONFIG_PROJECT_RESOURCES, xSyncConfig.getProjectResourceSyncType().getDisplayName());
        xnatDriver.exceptionSafeSelect(xsyncLocators.XSYNC_CONFIG_SUBJECT_RESOURCES, xSyncConfig.getSubjectResourceSyncType().getDisplayName());
        xnatDriver.exceptionSafeSelect(xsyncLocators.XSYNC_CONFIG_SUBJECT_ASSESSORS, xSyncConfig.getSubjectAssessorSyncType().getDisplayName());
        xnatDriver.exceptionSafeSelect(xsyncLocators.XSYNC_CONFIG_IMAGING_SESSIONS, xSyncConfig.getImagingSessionSyncType().getDisplayName());

        final XModal xsyncModal = new XModal(xsyncLocators.XSYNC_CONFIG_MODAL, xnatDriver);
        xsyncModal.uploadScreenshot();
        xnatDriver.exceptionSafeClick(locators.SUBMIT_BUTTON);

        final XModal xsyncCredentialsModal = new XModal(xsyncLocators.XSYNC_CREDENTIALS_MODAL, xnatDriver);
        xsyncCredentialsModal.captureStep();
        handleXSyncCredentialsModal(xSyncConfig.getDestinationXnat(), destinationUser);

        final XModal successModal = new XModal(xsyncLocators.XSYNC_SAVE_SUCCESS_MODAL, xnatDriver);
        successModal.captureStep();
        successModal.clickOK();
    }

    protected void provideSyncCredentials(String destinationXnat, User destinationUser) {
        xnatDriver.loadManageTab(xsyncLocators.XSYNC_MANAGE_TAB);
        exceptionSafeClick(xsyncLocators.REMOTE_CREDENTIALS_BUTTON);
        handleXSyncCredentialsModal(destinationXnat, destinationUser);
        final XModal successModal = new XModal(xsyncLocators.XSYNC_CREDENTIALS_SUCCESS_MODAL, xnatDriver);
        successModal.assertTextEquals("Successfully saved credentials for remote server " + destinationXnat);
        successModal.captureStep();
        successModal.clickOK();
    }

    protected void handleXSyncCredentialsModal(String destinationXnat, User destinationUser) {
        final XModal xsyncCredentialsModal = new XModal(xsyncLocators.XSYNC_CREDENTIALS_MODAL, xnatDriver);
        xnatDriver.fill(xsyncLocators.XSYNC_CREDENTIALS_USERNAME, destinationUser.getUsername());
        xnatDriver.fill(xsyncLocators.XSYNC_CREDENTIALS_PASSWORD, destinationUser.getPassword());
        xsyncCredentialsModal.assertTextContains("Enter credentials for: " + destinationXnat);
        xsyncCredentialsModal.uploadScreenshot();
        xsyncCredentialsModal.clickOK();
    }

    protected void launchProjectSync() {
        xnatDriver.actionsBoxNavigation("XSync", "Initiate Project Sync");
        final XModal confirmationModal = new XModal(xsyncLocators.PROJECT_SYNC_CONFIRM_MODAL, xnatDriver);
        confirmationModal.uploadScreenshot();
        confirmationModal.clickOK();
    }

    protected void projectSync() {
        launchProjectSync();

        assertEquals("Synchronization started.", driver.findElement(locators.ADMIN_UI_SAVE_SUCCESS).getText());
        try {
            captureStep(locators.ADMIN_UI_SAVE_SUCCESS);
        } catch (Exception e){
            captureScreenshotlessStep(); // If the save success banner disappears before we can screenshot it, just skip the screenshot
        }
        xnatDriver.waitForNotElement(locators.ADMIN_UI_SAVE_SUCCESS);
    }

    protected void viewCurrentSyncStatus() {
        xnatDriver.actionsBoxNavigation("XSync", "View Current Sync Status");
    }

    protected void waitForSyncCompletion(int maximumTime) {
        final StopWatch stopWatch = CommonUtils.launchStopWatch();

        while(true) {
            AssortedUtils.checkStopWatch(stopWatch, maximumTime, "Sync did not complete within maximum number of seconds: " + maximumTime);

            viewCurrentSyncStatus();
            final XModal statusModal = new XModal(xsyncLocators.SYNC_STATUS_MODAL, xnatDriver);
            final String status = xnatDriver.getNonemptyText(statusModal.getSubElementLocator(xsyncLocators.SYNC_STATUS_MODAL_CURRENT_STATUS));
            if (status.equals("Not Syncing")) {
                final String result = xnatDriver.getNonemptyText(statusModal.getSubElementLocator(xsyncLocators.SYNC_STATUS_MODAL_LAST_RESULT));
                if (result.equals("Successful")) {
                    statusModal.captureStep();
                    statusModal.clickOK();
                    driver.navigate().refresh();
                    break;
                } else {
                    fail("Sync did not complete successfully.");
                }
            }
            driver.navigate().refresh();
        }
    }

    protected void loadLatestSyncHistory() {
        xnatDriver.loadManageTab(xsyncLocators.XSYNC_MANAGE_TAB);
        xnatDriver.waitForElementVisible(xsyncLocators.XSYNC_HISTORY_LATEST_SYNC);
        uploadTargetedScreenshot(xsyncLocators.XSYNC_HISTORY_TABLE);
        xnatDriver.waitForProjectLoad();
        exceptionSafeClick(xsyncLocators.XSYNC_HISTORY_LATEST_SYNC);
    }

    protected void assertSyncResult(SyncResult expectedResult) {
        loadLatestSyncHistory();
        final XModal historyModal = new XModal(xsyncLocators.XSYNC_HISTORY_MODAL, xnatDriver);
        assertEquals(expectedResult.getOverallStatus().getDisplayName(), xnatDriver.getNonemptyText(xsyncLocators.XSYNC_HISTORY_OVERVIEW_STATUS));
        historyModal.uploadScreenshot();
        assertSubjectAssessors(expectedResult);
        historyModal.captureStep();

        exceptionSafeClick(locators.tagWithText("button", "Close"));
    }

    protected void assertSubjectAssessors(SyncResult expectedResult) {
        exceptionSafeClick(By.linkText("Subject Assessments"));

        if (expectedResult.getSessionResults().isEmpty()) {
            xnatDriver.waitForElement(xsyncLocators.XSYNC_HISTORY_NO_SUBJECT_ASSESSORS);
        } else {
            final By subjectAssessorTable = xsyncLocators.XSYNC_HISTORY_SUBJECT_ASSESMENT_TABLE;
            xnatDriver.waitForElement(subjectAssessorTable);
            final Map<ImagingSession, SessionStatus> sessionResults = new HashMap<>();
            sessionResults.putAll(expectedResult.getSessionResults());

            final int numSessionsPresent = driver.findElements(new ByChained(subjectAssessorTable, locators.TR_TAG)).size() - 1; // table header is not a session
            assertEquals(sessionResults.size(), numSessionsPresent);

            for (int i = 2; i <= numSessionsPresent + 1; i++) {
                final String sessionLabel = xnatDriver.exceptionSafeGetText(new ByChained(subjectAssessorTable, By.xpath(String.format(".//tr[%d]/td[@class='localLabel']", i))));
                final ImagingSession session = new ImagingSession(sessionLabel).project(expectedResult.getSourceProject());
                if (sessionResults.containsKey(session)) {
                    xnatDriver.assertElementText(sessionResults.get(session).getDisplayName(), new ByChained(subjectAssessorTable, By.xpath(String.format(".//tr[%d]/td[@class='syncStatus']", i))));
                    sessionResults.remove(session);
                }
            }
            assertTrue(sessionResults.isEmpty()); // All sessions must have been processed correctly to pass
        }
    }

    protected void assertSessionsSyncedAndVerified(Project sourceProject, ImagingSession... sessions) {
        SyncResult result = new SyncResult().sourceProject(sourceProject);
        for (ImagingSession session : sessions) {
            result.sessionResult(session, SessionStatus.SYNCED_AND_VERIFIED);
        }
        assertSyncResult(result);
    }

    // assumes you're already on the session page
    protected void manualSessionSync(ImagingSession session) {
        xnatDriver.waitForAsyncCalls();
        exceptionSafeClick(xsyncLocators.XSYNC_SESSION_TAB);
        exceptionSafeClick(xsyncLocators.MANUAL_SESSION_SYNC_BUTTON);

        final XModal confirmationModal = new XModal(xsyncLocators.MANUAL_SESSION_SYNC_MODAL, xnatDriver);
        confirmationModal.uploadScreenshot();
        confirmationModal.clickOK();

        final XModal requestSentModal = new XModal(xsyncLocators.MANUAL_SESSION_SYNC_REQUEST_SENT_MODAL, xnatDriver);
        requestSentModal.assertTextEquals(String.format("Sync request for this experiment (%s) has been sent.", session.getLabel()));
        requestSentModal.captureStep();
        requestSentModal.clickOK();
    }

    protected void locateSession(ImagingSession session, boolean captureStep) {
        if (session.getPrimaryProject() == null) fail("Project object must be set for the session.");
        if (session.getSubject() == null) fail("Subject object must be set for the session.");

        xnatDriver.findBySearch(session.getPrimaryProject().getId());
        xnatDriver.waitForProjectLoad();
        exceptionSafeClick(By.linkText(session.getSubject().getLabel()));
        exceptionSafeClick(locators.sessionLinkFromSubjectPage(session));
        if (captureStep) captureStep(locators.SESSION_PAGE);
    }

    protected void markOkToSync() {
        xnatDriver.waitForAsyncCalls();
        exceptionSafeClick(xsyncLocators.XSYNC_SESSION_TAB);
        xnatDriver.waitForElementVisible(xsyncLocators.OK_TO_SYNC_CHECKBOX);
        exceptionSafeClick(xsyncLocators.OK_TO_SYNC_CHECKBOX);

        final XModal successModal = new XModal(xsyncLocators.OK_TO_SYNC_MODAL, xnatDriver);
        successModal.captureStep();
        successModal.clickOK();
    }

    protected void locateSession(ImagingSession session) {
        locateSession(session, true);
    }


}
