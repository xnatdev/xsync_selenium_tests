package org.nrg.xnat.xsync.selenium;

import org.nrg.testing.xnat.conf.Settings;
import org.nrg.selenium.xnat.XNATLocators;
import org.nrg.selenium.xnat.XnatDriver;
import org.nrg.selenium.xnat.popups.PartialTextPopupLocator;
import org.nrg.selenium.xnat.popups.PopupLocator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.pagefactory.ByChained;

public class XSyncLocators {

    private XNATLocators locators = XnatDriver.getInstance(true, Settings.DEFAULT_XNAT_CONFIG).getLocators(); // Assumption that default config is source XNAT;

    public By XSYNC_MANAGE_TAB = locators.manageTab("XSync Configuration");
    public By BEGIN_CONFIGURATION = By.id("xsync-begin-config");
    public By REMOTE_CREDENTIALS_BUTTON = By.id("xsync-credentials");
    public By XSYNC_CONFIG_ENABLED = By.id("enabled");
    public By XSYNC_CONFIG_NEW_ONLY = By.id("xsync-config-newonly");
    public By XSYNC_CONFIG_DESTINATION_XNAT = By.id("xsync-config-remote-url");
    public By XSYNC_CONFIG_DESTINATION_PROJECT = By.id("xsync-config-remote-project");
    public By XSYNC_CONFIG_SYNC_FREQUENCY = By.id("sync_frequency");
    public By XSYNC_CONFIG_IDENTIFIERS = By.id("xsync-config-identifiers");
    public By XSYNC_CONFIG_ANONYMIZE = By.id("xsync-config-anonymize");
    public By XSYNC_CONFIG_PROJECT_RESOURCES = By.id("project_resources_sync_type_select_menu_id");
    public By XSYNC_CONFIG_SUBJECT_RESOURCES = By.id("subject_resources_sync_type_select_menu_id");
    public By XSYNC_CONFIG_SUBJECT_ASSESSORS = By.id("subject_assessors_sync_type_select_menu_id");
    public By XSYNC_CONFIG_IMAGING_SESSIONS = By.id("imaging_sessions_sync_type_select_menu_id");
    public By XSYNC_CONFIG_MODAL = locators.tagContainingText("span", "Project Sync Settings");
    public By XSYNC_CREDENTIALS_MODAL = locators.tagContainingText("span", "Enter remote server credentials");
    public By XSYNC_CREDENTIALS_USERNAME = By.xpath("//input[@name='username']");
    public By XSYNC_CREDENTIALS_PASSWORD = By.xpath("//input[@name='password']");
    public PopupLocator XSYNC_CREDENTIALS_SUCCESS_MODAL = new PartialTextPopupLocator("Successfully saved credentials");
    public PopupLocator XSYNC_SAVE_SUCCESS_MODAL = new PartialTextPopupLocator("The XSync configuration has been saved");
    public By PROJECT_SYNC_CONFIRM_MODAL = locators.tagContainingText("span", "Confirm Project Sync");
    public By PROJECT_SYNC_ERROR_BANNER = By.xpath("//div[@class='banner top-banner error']");
    public By SYNC_STATUS_MODAL = locators.tagContainingText("span", "Synchronization status");
    public By SYNC_STATUS_MODAL_CURRENT_STATUS = By.xpath(".//b[text()='Current Status: ']/../following-sibling::td/b");
    public By SYNC_STATUS_MODAL_LAST_RESULT = By.xpath(".//td[text()='Last Sync Result: ']/following-sibling::td");
    public By XSYNC_HISTORY_TABLE = By.id("xsync-history-table");
    public By XSYNC_HISTORY_LATEST_SYNC = new ByChained(XSYNC_HISTORY_TABLE, By.xpath(".//tr//a[not(@title < ../../../..//a/@title)]")); // most recent link is sadly not always latest sync. Instead, this locator hacks together a maximum selector on the title attributes for the links
    public By XSYNC_HISTORY_MODAL = locators.tagContainingText("span", "Xsync History");
    public By XSYNC_HISTORY_OVERVIEW_STATUS = By.xpath(".//div[@id='sync-status-element']/div[@class='element-wrapper']");
    public By XSYNC_HISTORY_SUBJECT_ASSESMENT_TABLE = By.xpath("//table[@id='subject assessments-table']");
    public By XSYNC_HISTORY_NO_SUBJECT_ASSESSORS = By.xpath("//div[@data-tab='subject-assessments-tab']//i[text()='Nothing synced.']");
    public By XSYNC_SESSION_TAB = locators.tab("Synchronization");
    public By MANUAL_SESSION_SYNC_BUTTON = By.xpath("//input[@value='Sync This Session Now']");
    public By MANUAL_SESSION_SYNC_MODAL = locators.tagWithText("span", "Confirm Manual Sync");
    public By MANUAL_SESSION_SYNC_REQUEST_SENT_MODAL = locators.tagWithText("span", "Data Synchronization");
    public By OK_TO_SYNC_CHECKBOX = By.id("OkToSync");
    public PopupLocator OK_TO_SYNC_MODAL = new PartialTextPopupLocator("marked OK to sync");

}
