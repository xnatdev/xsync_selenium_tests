package org.nrg.xnat.xsync.selenium.enums;

public enum Identifiers {
    USE_LOCAL("Local", "use_local");

    private String displayName;
    private String jsonName;

    Identifiers(String displayName, String jsonName) {
        this.displayName = displayName;
        this.jsonName = jsonName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getJsonName() {
        return jsonName;
    }

}
