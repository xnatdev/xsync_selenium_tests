package org.nrg.xnat.xsync.selenium.enums;

public enum SyncFrequency {
    ON_DEMAND ("On Demand", "on demand"),
    HOURLY    ("Hourly", "hourly"),
    DAILY     ("Daily", "daily"),
    WEEKLY    ("Weekly", "weekly"),
    MONTHLY   ("Monthly", "monthly");

    private String displayName;
    private String jsonName;

    SyncFrequency(String displayName, String jsonName) {
        this.displayName = displayName;
        this.jsonName = jsonName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getJsonName() {
        return jsonName;
    }

}
