package org.nrg.xnat.xsync.selenium;

import org.nrg.xnat.pojo.Project;
import org.nrg.xnat.pojo.experiments.ImagingSession;
import org.nrg.xnat.xsync.selenium.enums.OverallStatus;
import org.nrg.xnat.xsync.selenium.enums.SessionStatus;

import java.util.HashMap;
import java.util.Map;

public class SyncResult {

    private OverallStatus overallStatus = OverallStatus.COMPLETE_AND_VERIFIED;
    private Map<ImagingSession, SessionStatus> sessionResults = new HashMap<>();
    private Project sourceProject;

    public SyncResult overallStatus(OverallStatus overallStatus) {
        this.overallStatus = overallStatus;
        return this;
    }

    public SyncResult sessionResult(ImagingSession session, SessionStatus status) {
        sessionResults.put(session, status);
        return this;
    }

    public SyncResult sourceProject(Project project) {
        this.sourceProject = project;
        return this;
    }

    public OverallStatus getOverallStatus() {
        return overallStatus;
    }

    public Map<ImagingSession, SessionStatus> getSessionResults() {
        return sessionResults;
    }

    public Project getSourceProject() {
        return sourceProject;
    }

}
